# topn
"topn" finds the largest n numbers in a file of any size. It uses a Rust BufReader to read the file one chunk at a time to ensure stable memory usage throughout the search and a minimum binary heap to keep track of the highest numbers.

## Complexity

### Run Time (Worst Case)
`O(k log n)`
- k is the number of rows in the file (reading)
- n is the number of highest numbers to keep track of (O(log n) insertion in binary heap)

### Space
`O(n)`
- n is the number of highest numbers to keep track of

## Improvement
To improve upon my method, I would do parallel processing of the original file in a quicksort algorithm that uses files to store its partitioning results recursively until the partitions of data can safely fit into memory for final in-memory quick-sortings that would again save to files. This approach assumes as much drive space is available as the original data file and that solid state drives are being used. Solid state drives support multithreaded reads through the pread function (C) and would allow several threads to read the file simultaneously along the quicksort partitions.

1. Find a random number at the center of the original file to pivot on
2. Create two pread() threads on data before and after this pivot to read and write the values higher and lower into two new files, appropriately labelled
3. Keep repeating this quicksort-with-files algorithm, generating additional threads and preads
4. As each successive quicksort level is complete, the previous quicksort file can be safely deleted
5. As smaller and smaller files are generated, a single file will be able to eventually fit in memory
6. Using the now in-memory quicksort partitions, complete the quicksort in memory and again save the final results to files
7. The sum of the sizes of the final sets of files should equal the original file, but fully sorted
8. The top n numbers can be read directly from these sets of files, assuming they were names sequentially

## Execution

### Assumptions
- MacOS / Linux / Windows
- Rust installed (https://www.rust-lang.org/tools/install)

### Building
- `cargo build --release`
- binaries are put in the target/release folder

### Testing
- `cargo test`
- only one test is present

### Using
`topn -n {number} -b {buffer capacity} file`
- n is the number of max numbers to find
- b is the max memory used by the file reading process
- file is a list of unsigned 64-bit numbers (max)