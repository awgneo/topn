#![feature(try_from)]

use std::io::prelude::*;
use std::fs::OpenOptions;
use std::process;
use std::convert::TryFrom;
use std::io::BufReader;
use binary_heap_plus::*;

pub fn search(file: &str, n: u64, b: u64) -> Vec<u64> {

    let top_numbers_size = usize::try_from(n).unwrap_or_else(|error| {
        println!("Could not create top numbers size: {}", error);
        process::exit(1);
    });

    // open file for reading
    let file = OpenOptions::new().read(true).open(file).unwrap_or_else(|error| {
        println!("Could not open file: {}", error);
        process::exit(1);
    });

    let buffer_size = usize::try_from(b).unwrap_or_else(|error| {
        println!("Could not create buffer size: {}", error);
        process::exit(1);
    });

    println!("Starting search for top {} numbers with {} buffer size", top_numbers_size, buffer_size);

    // allocate a buffer reader to read in the file
    let reader = BufReader::with_capacity(buffer_size, file);
    // allocate min binary heap for the top numbers
    let mut top_numbers: BinaryHeap<u64, MinComparator> = BinaryHeap::with_capacity_min(top_numbers_size);

    for line in reader.lines() {

        // read line
        let line = line.unwrap_or_else(|error| {
            println!("Failed to read line: {}", error);
            process::exit(1);
        });

        // parse number
        let number = line.parse().unwrap_or_else(|error| {
            println!("Failed to parse line '{}' into number: {}", line, error);
            process::exit(1);
        });

        // keep adding numbers until we are at capacity
        if top_numbers.len() < top_numbers_size {
            top_numbers.push(number);
            continue;
        }

        // get the top min number
        match top_numbers.peek() {

            Some(top_number) => {
                // compare to current number
                if number > *top_number {
                    // push new top number on to heap
                    top_numbers.pop();
                    top_numbers.push(number);
                }
            },

            None => {
                top_numbers.push(number);
                continue;
            }

        };
        
    }

    // return sorted vector
    top_numbers.into_sorted_vec()
    
}

#[cfg(test)]
mod tests {
    #[test]
    fn search() {
        use super::*;
        let top_numbers: Vec<u64> = search("numbers.txt", 5, 8192);
        assert_eq!(top_numbers.len(), 5);
        assert!(top_numbers.contains(&568678678967));
        assert!(top_numbers.contains(&5435634));
        assert!(top_numbers.contains(&987098));
        assert!(top_numbers.contains(&65346));
        assert!(top_numbers.contains(&9789));
    }
}