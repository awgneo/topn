#[macro_use]
extern crate clap;
extern crate topn;

use std::process;

fn main() {

    // setup command line interface
    let matches = clap_app!(myapp =>
        (version: "1.0")
        (author: "Alex Groleau <agroleau@teampalmtree.com>")
        (about: "Calculates the top N largest numbers efficiently in a file")
        (@arg n: -n +takes_value "Top numbers in file (default 10)")
        (@arg b: -b +takes_value "Buffer size to read each chunck of the file (default 8192)")
        (@arg file: +required "File to use")
    ).get_matches();

    // get method, threads, and file
    let n: u64 = value_t!(matches.value_of("n"), u64).unwrap_or(10);
    let b: u64 = value_t!(matches.value_of("b"), u64).unwrap_or(8192);
    let file = matches.value_of("file").unwrap_or_else(|| {
        println!("Missing file");
        process::exit(1);
    });

    // find top n numbers
    let top_numbers: Vec<u64> = topn::search(file, n, b);
    println!("Top n numbers: {:?}", top_numbers);

}